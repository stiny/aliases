SHELL=/bin/bash
.ONESHELL:
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
init: t1 wait
	# $(MAKE) -f ${mkfile_path} wait
	$(MAKE) -f ${mkfile_path} t1
wait:
	# wait before parallel tasks
	sleep 1
	echo wait 1s
t%:
	# sleep task
	@$(eval sec=$(shell echo $@ | cut -c 2-))
	sleep ${sec}
	echo ${sec}
