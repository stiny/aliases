ulocal_extra_tunnel:=-L 2375:localhost:2375 -L 8081:localhost:8081 -L 8002:localhost:80 -L 2020:localhost:2020 -L 8123:localhost:8123
uzhang_extra_tunnel:=-L 8081:localhost:8081
uprod_extra_tunnel:=-
alilzf_extra_tunnel:=-L 2376:localhost:2375
# tencent_extra_tunnel:=-L 8123:localhost:8123
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
deps:=alilzf uprod 
.SUFFIXES:
.PHONY: restart
# .ONESHELL:
SHELL := /bin/bash
# .SHELLFLAGS = -ex -c
test:
	echo ${SHELLOPTS}
	echo ${SHELL}
	echo 123
	echo ${mkfile_path}
restart: $(deps:%=%_restart)
# restart: uprod_restart alilzf_restart
# restart: uprod_restart
	@echo
start: $(deps:%=%_start)
	@echo
stop: $(deps:%=%_stop)
	@echo
status: $(deps:%=%_status)
	@echo
%_restart: %_define %_stop %_start
	# $(MAKE) -f ${mkfile_path} ${host}_stop
	# $(MAKE) -f ${mkfile_path} ${host}_start
%_start: %_define
	ssh ${host} 'fuser -k 9000/tcp | true'
	autossh -M 0 -fnN ${host} -R dockerhost:9000:localhost:9000 ${${host}_extra_tunnel}
%_stop: %_define
	ps -ef | grep autossh | grep ${host} | grep -v grep | awk '{print $$2}' | xargs -I '{}' bash -c 'kill -SIGTERM -- -{}'
%_status: %_define
	ssh ${host} netstat -lntp 2>/dev/null
%_define: ${snippets_dir}/ip/%
	$(eval host=$(*F))
