select extract('hour' from created_at),count(0)
from tasks t
where 1=1
and t.created_at::date=current_date
group by extract('hour' from created_at)
order by extract('hour' from created_at)