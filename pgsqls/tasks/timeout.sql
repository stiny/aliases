SELECT a.node_id,t.*
FROM public.tasks t
inner join accounts a
on t.account_id=a.id
WHERE t.created_at::date=current_date
and t.status in (-3,1,0)
order by t.created_at