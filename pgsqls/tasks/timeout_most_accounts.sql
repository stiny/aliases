SELECT a.id,count(0)
FROM public.tasks t
inner join accounts a
on t.account_id=a.id
WHERE 1=1
and t.created_at::date=current_date
and t.status=1
group by a.id
order by count(0) desc