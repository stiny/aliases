SELECT a.node_id,t.status=1,count(0)
FROM public.tasks t
inner join accounts a
on t.account_id=a.id
WHERE t.created_at::date=current_date
group by a.node_id,t.status=1
