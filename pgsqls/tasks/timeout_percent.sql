SELECT t.created_at::date as "日期",
count(0) as "task总数",
sum(case when t.status in (-3,1) then 1 else 0 end) as "超时总数",
round(sum(case when t.status in (-3,1) then 1 else 0 end)*100.0/count(0)::numeric, 2) as "超时率"
FROM public.tasks t
inner join accounts a
on t.account_id=a.id
WHERE 1=1
-- and t.created_at::date=current_date
group by t.created_at::date
order by t.created_at::date desc