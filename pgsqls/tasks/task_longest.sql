select updated_at-created_at,*
from tasks t
where 1=1
and t.created_at::date=current_date
order by updated_at-created_at desc
limit 200