#!/bin/bash
export shell_dir="$HOME/configs"
export plugins_dir="$HOME/configs/plugins"
export snippets_dir="${shell_dir}/snippets"
source ${snippets_dir}/exports
source ${snippets_dir}/autocomplete

if [[ "$-" =~ i ]]; then
  stty -ixon
  source ${snippets_dir}/inputrc
fi

# bash alias file
for _alias in $shell_dir/alias/*.alias.sh ; do
    . $_alias
done

# bash function file
for _fun in $shell_dir/function/*.fun.sh ; do
    . $_fun
done

### export XDG_CONFIG_HOME="${shell_dir}"

# for root git pull etc.
umask 002
