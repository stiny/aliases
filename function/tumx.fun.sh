#!/usr/bin/env bash

function tml() {
  local _tmux=\tmux" -f ${shell_dir}/.tmux.conf -S ${snippets_dir}/.ignore_files/tmux.sock"
  if [[ -e ${snippets_dir}/.ignore_files/tmux.sock ]]; then
    $_tmux ls &>/dev/null
    if ! [[ $? -eq 0 ]]; then
      \rm ${snippets_dir}/.ignore_files/tmux.sock
    elif [[ "$1" ]]; then
      $_tmux attach -t $1
    else
      big=$($_tmux ls 2>/dev/null | tail -1 | cut -d' ' -f1)
      $_tmux attach -t $big
    fi
  elif [[ "$1" ]] && [[ -n $(tmls) ]]; then
    $_tmux attach -t $1
  fi
}
function tmll() {
  local _tmux=\tmux" -f ${shell_dir}/.tmux.conf -S ${snippets_dir}/.ignore_files/tmux.sock"
  if [[ -e ${snippets_dir}/.ignore_files/tmux.sock ]]; then
    $_tmux ls 2>/dev/null
    if ! [[ $? -eq 0 ]]; then
      \rm ${snippets_dir}/.ignore_files/tmux.sock
    fi
    if [[ $# -gt 0 ]]; then
      $_tmux $*
    else
      $_tmux
    fi
  else
    $_tmux
  fi
}
function tmls() {
  local _tmux=\tmux" -f ${shell_dir}/.tmux.conf -S ${snippets_dir}/.ignore_files/tmux.sock"
  if [[ -e ${snippets_dir}/.ignore_files/tmux.sock ]]; then
    $_tmux ls 2>/dev/null
    if ! [[ $? -eq 0 ]]; then
      \rm ${snippets_dir}/.ignore_files/tmux.sock
    fi
  fi
}
function tmks() {
  local _tmux=\tmux" -S ${snippets_dir}/.ignore_files/tmux.sock"
  local i
  for i in "$@"
  do
    $_tmux kill-session -t $i
  done
}
