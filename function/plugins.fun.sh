#!/usr/bin/env bash

if [[ -x "${shell_dir}/plugins/.bin/sempl" ]]; then
  if [[ -z $(which sempl) ]]; then
    export PATH="${shell_dir}/plugins/.bin:${PATH}"
  fi
fi
if [[ -x "/usr/local/go/bin/go" ]]; then
  if [[ -z $(which go) ]]; then
    export PATH="${shell_dir}/plugins/.bin:${PATH}"
  fi
fi
alias rebinall="rebin $( ls ${shell_dir}/plugins )"
function rebins() {
  gsmu
  if [[ ! -e "${plugins_dir}/.bin" ]]; then
    mkdir -p "${plugins_dir}/.bin"
  fi
  ln -sf "${plugins_dir}/sempl/sempl" "${plugins_dir}/.bin/sempl"
  ln -sf "${plugins_dir}/sempl/crypttool" "${plugins_dir}/.bin/crypttool"
  cp -a "${plugins_dir}/nginx-modsite/nginx-modsite" "${plugins_dir}/.bin/nginx-modsite" 2>/dev/null && chmod +x "${plugins_dir}/.bin/nginx-modsite"
}
function rebin() {
  cat ${snippets_dir}/ln_in_plugin | /bin/bash -s -- "$@"
}
