#!/usr/bin/env bash
function gls() {
  local dir=${1-} options
  if [[ -n $dir ]]; then
    shift
    options="--name-only $*"
    cd $dir
    git ls-tree $options HEAD
    cd - &>/dev/null
  else
    git ls-tree --name-only HEAD
  fi
}
function glsa() {
  git ls-files -o
}
function glso() {
  local _opts
  _opts=${*---exclude-standard}
  git ls-files -o ${_opts}
}
function glsm() {
  git ls-files -m
}
function glsot() {
  local _files
  if [[ -e $1 ]]; then
    tail -n +1 -- $1 | less
  elif [[ -n $1 ]]; then
    _files=$(glso | grep $1)
    tail -n +1 -- $_files | less ${_pattern---pattern='==>.*<=='}
  else
    tail -n +1 -- $(glso) | less ${_pattern---pattern='==>.*<=='}
  fi
}
