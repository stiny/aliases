#!/usr/bin/env bash

function dex() {
  local ctr_name cmd
  ctr_name=$1
  shift
  if [[ $# -eq 0 ]]; then
    cmd='bash'
  else
    cmd="$@"
  fi
  # $@ not split by space
  docker exec -it $ctr_name "$cmd"
}
