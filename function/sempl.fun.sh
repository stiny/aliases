#!/usr/bin/env bash

# templaet snippets
alias tpl='sempl -o -f'
function stpl() {
  tpl "${@:2}" <(gets "$1")
}
function srt() {
  . <(tpl "${@:2}" <(gets "$1"))
}
function srs() {
  set -o pipefail
  . ${snippets_dir}/$1 ${@:2}
  set +o pipefail
}
function srd() {
  read -s -p 'password:' password
  . <(crypttool -p "$password" cat "${snippets_dir}/$1")
}