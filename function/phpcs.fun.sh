#!/usr/bin/env bash

function pcf() {
  local _dir
  _dir="${*:-.}"
  php-cs-fixer fix --config ${shell_dir}/.php_cs --allow-risky yes $_dir
}
