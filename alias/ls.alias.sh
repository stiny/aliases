#!/usr/bin/env bash

alias l='ls -al'
alias ll='ls -l'
alias lh='ls -alh'
alias la='ll -a'
alias lsd='ls -l | grep ^d'
alias clr='clear'
alias cls='clear; ls'
alias cll='clear; ls -al'
alias cla='clear; ls -a'
