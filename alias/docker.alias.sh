#!/usr/bin/env bash

# docker #
alias dup="docker-compose up --remove-orphans -d"
alias dupc="docker-compose up --remove-orphans --force-recreate -d"
alias dcm="docker-compose"
alias dim="docker images"
alias din='docker inspect'
alias dlg='docker logs'
alias dps='docker ps'
alias drm='docker \rm $(docker ps -aq -f "status=exited")'
alias drmi='docker rmi'
alias down='docker-compose stop'
alias dst='docker-compose stop'
alias dstp='docker stop $(docker ps -q --filter status=running)'
alias dtp='docker top'
alias dip='docker inspect -f='"'"'{{.Name}} - {{.NetworkSettings.IPAddress}}'"'"' $(docker ps -a -q)'
alias dnet='docker network'
alias dtmp='docker run --rm -it --name test'
