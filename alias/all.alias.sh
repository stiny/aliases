#!/usr/bin/env bash
#vim
export VISUAL="vim -u ${shell_dir}/.vimrc"

#nvim
alias nv="nvim -u $shell_dir/init.vim"

#history
alias hist="HISTTIMEFORMAT=$'\r' history"
alias his="history | tail -100"
alias ext="unset HISTFILE && logout"

#ansible
alias ag="ansible-galaxy"
#alias ap="ANSIBLE_NOCOWS=1 ansible-playbook -i ${ansible_dir}/hosts"
#alias ansible="ansible -i ${ansible_dir}/hosts"

#mysql
alias mysqldump_with_options="mysqldump --add-drop-table --complete-insert --single-transaction --set-gtid-purged=OFF --skip-comments"

#apt
alias aiy='apt install -y'

# reaload aliases.sh
alias rea="source ${shell_dir}/aliases.sh && echo 'reloaded'"

#cd
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

#composer
alias cmp='composer'
alias cmc='composer config'
alias cdp="cmp dumpautoload"

#swap
alias addswap='stpl swapfile_mk'
alias delswap='stpl swapfile_rm'
complete -W "$(eval "ls /var/_swap_ 2>/dev/null | xargs")" addswap delswap

alias sudo='sudo ' #https://askubuntu.com/questions/22037/aliases-not-available-when-using-sudo#22043

alias ssh="ssh -F $shell_dir/ssh_config"
alias lsc='ln -s ~/configs/ssh_config ~/.ssh/config 2>/dev/null;chmod 600 ~/.ssh/config'

alias gmt='git mergetool'
