# handy bash with some aliases and functions

## 代码片段
对于比较长，容易输错的代码片段，保存起来，便于后期执行，还可以设置一些变量
- 添加片段
```bash
sets php/cmp composer config ${g-} repo.packagist composer https://mirrors.aliyun.com/composer/
```
- 查看片段
```bash
gets php/cmp
```
- 执行片段
```bash
srs php/cmp
g=-g srs php/cmp  # for global
```